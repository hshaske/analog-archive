import React from 'react'
import { withItem } from './ItemContext'
import Audio from './Audio'
import Video from './Video'
import Gallery from './Gallery'
import Paper from './Paper'

const Media = (props) => {
  const { itemType } = props.item

  const renderMediaPlayer = () => {
    switch (itemType) {
      case 'agenda':
        return <Paper />
      case 'review':
        return <Paper />
      case 'ephemera':
        return <Paper />
      case 'gallery':
        return <Gallery />
      case 'video':
        return <Video />
      default:
        return <Audio />
    }
  }

  return renderMediaPlayer()
}

export default withItem(Media)

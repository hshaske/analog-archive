import React, { Component } from 'react'
import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { styles as s } from 'stylesheet'
import { Column, Row } from 'components/layout'
import Markdown from 'components/Markdown'
import { withItem } from './ItemContext'

const iFrameWrapper = styled.div({
  img: css(s.block, {
    width: 'auto',
    margin: '0 auto',
  }),

  '.caption-wrapper': {
    marginTop: '1rem',
  },

  '.gallery-track': {
    paddingBottom: '3rem',
  },

  button: {
    cursor: 'pointer',
  },

  '.hero-media': {
    img: {
      maxHeight: '50vh',
    },
  },

  '.slider-selector': css( {
    paddingTop: '0.5rem',
    paddingBottom: '0.5rem',
  }),

  '.selector-option': css(s.block, {
    cursor: 'pointer',

    img: {
      border: '3px solid transparent',
    },

    '&.active': {
      img: {
        border: '3px solid white',
      },
    },
  }),
})

const StyledGallery = styled.div({
  img: css(s.block, {
    width: 'auto',
    margin: '0 auto',
  }),

  '.gallery-track': {
    paddingBottom: '3rem',
  },

  button: {
    cursor: 'pointer',
  },

  '.hero-media': {
    img: {
      maxHeight: '50vh',
    },
  },

  '.slider-selector': css( {
    paddingTop: '0.5rem',
    paddingBottom: '0.5rem',
  }),

  '.selector-option': css(s.block, {
    cursor: 'pointer',

    img: {
      border: '3px solid transparent',
    },

    '&.active': {
      img: {
        border: '3px solid white',
      },
    },
  }),
})

class Paper extends Component {
  renderImage = ({ caption, image }) => (
    <Row className='hero-media' middle='xs'>
      <Column xs={caption ? 9 : 12}>
        <img alt={caption} src={image} />
      </Column>
      {caption && (
        <Column xs={3}>
          <Markdown>{caption}</Markdown>
        </Column>
      )}
    </Row>
  )

  render = () => {
    const { item } = this.props
    const { media } = item

    if (!item || !item.pdfembed) {
      if (media.length === 1) {
        const item = media[0]

        if (!item || !item.image) {
          return <div><span /></div>
        }

        return <StyledGallery>{this.renderImage(item)}</StyledGallery>
      }
      return <div><span /></div>
    } 

    return (
      <iFrameWrapper>
        <iframe title={item.title} src={'https://archive.org/stream/' + item.pdfembed} width='100%' height='800px' />
      </iFrameWrapper>
    )
  }
}

export default withItem(Paper)

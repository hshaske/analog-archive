import React from 'react'
import { Row as RowComponent } from 'react-flexbox-grid'

const Row = props => <RowComponent {...props} />

export default Row

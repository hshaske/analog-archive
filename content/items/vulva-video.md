---
title: 'Vulva Video Collective '
itemType: gallery
poster: >-
  /archive/media/venas-de-las-mujeres-exhibit-wb-1976-judy-baca-and-janice-yudell.jpg
images:
  - caption: >-
      Vulva Video group members Janice Yudell and Judy Baca at the Woman's
      Building, 1976
    image: >-
      https://archive.org/download/venas-de-las-mujeres-exhibit-wb-1976-judy-baca-and-janice-yudell/Venas%20de%20las%20mujeres%20exhibit%2C%20WB%201976%20-%20Judy%20Baca%20and%20Janice%20Yudell_thumb.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
INTERVIEW (with Janice Yudell):

I got a BS in art at Univ. of Wisc Madison and when I moved to Berkeley for the next year, 1968-69, became interested in experimental filmmaking, so I  got a regular 8mm windup camera and took a filmmaking class at the San Francisco Art Institute with Ben Meeker. I attended Canyon Cinema showings in SF every Thursday night and especially loved Stan Brackage and also Maya Deren.  Brackage's short fast images were compelling - made filmmaking like a puzzle - and I shot short and fast images, kind of like him, partly trying to edit in the camera.  Then I moved to Venice, CA and joined a group called the LA Public Access Project.  They had just gotten a grant from the Episcopalian Church to teach and do portable video in the community.  I loved the idea -- this was instant TRUTH.  The Holocaust was what motivated me in trying to be a truth teller.  I taught some basic workshops, taped feminist activities, and started learning about community video channels/.communications.  I always wanted to combine art and activism - political images/ideas but not as narrative as in strict documentary.  But I teamed up with Judy Goldberg and we basically documented all kinds of feminist activities - I forget if we had a name...We made a short about the Silverlake People's Playgroup  and Carol Downer's pelvic self-examination.  Then the first Lesbian Conference came along and we joined with 4 others, Judy Baca, Frances Reid, Quathryn Brehm and Christie Schlesinger to become Vulva Video but only did this one tape together.  Judy and I did 2/3 of the editing. 

The feminist world opened up to me - I learned so much about people, issues, and art.  I also was involved in Feminist Art - I was a painter/drawer and my ideas/art images evolved..

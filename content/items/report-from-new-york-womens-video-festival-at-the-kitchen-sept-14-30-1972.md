---
title: >-
  Report from New York: Women's Video Festival at The Kitchen (Sept. 14-30,
  1972)
itemType: review
poster: /archive/media/women-film-cover-photo.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - {}
pdfembed: 1972-report-from-ny-women-and-film/mode/2up
---
"Report from New York: Women's Video Festival at The Kitchen (Sept. 14-30, 1972)" 

Jeanne Betancourt

Women & Film, vol. 1, nos. 3 & 4

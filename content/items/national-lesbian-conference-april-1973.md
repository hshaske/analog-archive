---
title: National Lesbian Conference April 1973
itemType: video
images:
  - caption: 'Jan Oxenberg filming '
    imageUpload: /archive/media/donnajanoxfilming.jpg
collections:
  - {}
resources:
  - file: ''
keywords:
  - {}
related:
  - items:
      - {}
annotations:
  - body: |-
      Jan Oxenberg Filming at the conference

      Photo: Christina Schlesinger
    end: '00:00:00'
    files: []
    images:
      - image: /archive/media/donnajanoxfilming.jpg
    start: '00:00:00'
  - body: Conference Crowds
    end: '01:00:00'
    images:
      - image: /archive/media/crowdscene4.jpg
    start: '01:00:00'
  - body: Topless conference attendees
    end: '02:00:00'
    images:
      - image: /archive/media/lesboconfnude.jpg
    start: '02:00:00'
  - body: Qathryn Brehm from Vulva Video filming
    end: '03:00:00'
    images:
      - image: /archive/media/qbvulvavideo.jpg
    start: '03:00:00'
---
Vulva Video

---
title: 1972 Agenda
itemType: agenda
poster: /archive/media/1972-agenda_0000.jpg
images:
  - caption: 1972 Agenda
    image: >-
      https://archive.org/download/1972agenda/1972%20-%20agenda_jp2.zip/1972%20-%20agenda_jp2%2F1972%20-%20agenda_0000.jp2&ext=jpg
collections:
  - value: ''
resources: []
keywords:
  - {}
related:
  - items:
      - {}
agendafiles:
  - {}
pdfembed: counterculturereviewofwomensvideofestival1972
---
This is the 1972 Agenda for the Women's Video Festival. Here's where the description would go!

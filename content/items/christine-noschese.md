---
title: 'Christine Noschese '
itemType: gallery
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Christine Noschese is the writer, director and producer of  documentary and fictional films that reflect her working class background and feminist perspective. Her films include Keep on Steppin’, which won Best Short at the Newberry Port Documentary Festival and was selected for screening at festivals in the U.S. and abroad, including BET’s Urban World. Her short film, June Roses, premiered in New Directors/New Films at the Museum of Modern Art and was screened at festivals nationwide. Christine’s documentary, Metropolitan Avenue, was broadcast nationally on PBS’ P.O.V. and on Channel Four in England. It had a theatrical run at the Film Forum in NYC. She received the John Grierson award for Best Director and an Emmy nomination as Best Director. More than 2,000 copies of the film have been distributed by the John T. and Catherine D. MacArthur Foundation to libraries throughout the U.S.

Christine was a directing fellow at the American Film Institute’s Center for Film and Television Studies and received an M.A. from Goddard College. She is a Professor of Film at Hofstra University.

Christine’s short comedy, Mary Therese, won her a New York Foundation Arts Fellowship in Filmmaking. Women of Northside Fight Back, the documentary she co-produced, was selected for the Women’s Video Festival in NY and their screenings abroad. In addition to her independent films and videos, Christine produced and directed educational videos for unions, educational institutions and non-profit organizations. She was a founding member and Director of the National Congress of Neighborhood Women, where she documented the work of women activists on video in the Greenpoint/Williamsburg community in Brooklyn. Christine’s early video work is archived in the Sofia Smith Collection at Smith College.

Christine’s work has been supported by the New York State Council on the Arts, the National Endowment for the Humanities, the New York Council on the Humanities, the Ford Foundation, the American Film Institute’s Independent Filmmaker’s Program and the Women in Film Fund, among others. Her films were critically acclaimed by the New York Times, the Los Angeles Times, Variety, MS Magazine, Sight and Sound, The American Anthropologist, etc.

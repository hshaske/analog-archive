---
title: 'Carol Vontobel '
itemType: gallery
poster: /archive/media/carol-vontobel-profile-pic.jpg
images:
  - image: >-
      https://archive.org/download/carol-vontobel-profile-pic/Carol%20Vontobel%20-%20profile%20pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
During her tenure with Media Bus / Videofreex, Carbol Vontobel helped produce live programs for their local show on Lanesville TV in upstate New York. These loosely-structured broadcasts were a mix of live and taped segments featuring video work by epopel from around the world; dramatic and comic pieces staring local residents; guest appearances by the famous and the not-so-famous and an open on-air phone line for calls while the programs were in progress. Her video A Portapak Conversation (1973), produced collaboratively while working with Videofreex, was screened as part of the Women’s  Video Festival in Buffalo, New York City, and San Francisco.

For the last several years, Carol Vontobel worked as an educator and as a caseworker for the developmentally disabled.

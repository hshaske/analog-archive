---
title: 'Charlotte Moorman Performances '
itemType: gallery
poster: /archive/media/charlotte-moorman-1973-performances.jpg
images:
  - image: /archive/media/charlotte-moorman-1973-performances.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Includes: 

"Crotch Music" by Jim McWilliams

"C. Moorman in Drag" by Jim McWilliams

"Chamber Music" by Takehisa Kosugi with videotapes by Ed Emshwiller

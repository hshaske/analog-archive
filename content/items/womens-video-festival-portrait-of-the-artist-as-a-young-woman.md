---
title: 'Women''s Video Festival: Portrait of the Artist as a Young Woman'
itemType: review
poster: /archive/media/portrait-of-the-artist-.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - {}
pdfembed: 1976-portrait-of-the-artist-as-a-young-woman-article
---
"Women's Video Festival: Portrait of the Artist As a Young Woman" 

Kate Walter

The Aquarian 

June 30 - July 14, 1976

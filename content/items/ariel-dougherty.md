---
title: 'Ariel Dougherty '
itemType: gallery
poster: /archive/media/ariel-dougherty-profile-pic.png
images:
  - caption: 'Ariel Dougherty with a Bolex 16mm camera '
    image: /archive/media/ariel-dougherty-profile-pic.png
  - caption: >-
      Video project taught by Ariel Dougherty with Mothers at PS 33, Fall
      1972-Spring 1973, a community outreach program of Women Make Movies.
    image: /archive/media/ariel-ps126.jpg
  - caption: 'Still image from SONGS, SKITS, POETRY and PRISON LIFE'
    image: /archive/media/ariel-dougherty-songs-oreo-cookie.jpg
collections:
  - {}
resources:
  - file: >-
      /archive/media/amazon-media-project-women-for-women-by-becky-hahnert-off-our-backs.jpg
    title: Testing
keywords:
  - value: Test Keyword
  - value: Another Keyword
related:
  - items:
      - {}
---
Ariel Dougherty for over fifty years has brought a feminist vision to her work on media justice and cultural diversity. An independent filmmaker and visual artist, she is a co-founder of Women Make Movies Inc, today the globe's premier distributor of women's films. The central focus of the organization at its start in 1972 was operation of a community based media workshop for women in the economically and racially mixed Chelsea NYC neighborhood and establishing an indispensable distribution service that was also a necessary earned income program. As outreach beyond the workshop in those early years she worked with a group of mothers at a local elementary school teaching video production. She also taught a special 12 week video program at Bedford Hills Correctional Facility for Women in the Summer of 1973. After organizing a national conference of Feminist Film and Video Organizations in 1975 she was part of an evolving network that created International VIDEOLETTERS a bi-monthly women's communities' video news exchange. Ultimately the network involved 27 women's video groups from 17 feminist communities. On a wing and prayer it lasted two full years, 1975-1977, on volunteer energy. Maybe 60 half-hour tapes were produced. Seven tapes currently have been found and digitized. A handful of other tapes may be in the wings of becoming available for contemporary audiences and academic study.



During most of the 1980s Ariel curated “Women's Work in Film and Video” at Women's Studio Workshop in Rosendale, NY where she served as development director. Late in the decade and into the mid 90's Ariel did development work at Local TV, the East Hampton, NY public access facility where she also organized the community producers into a support network. During this time she anchored her own program Cultural Ecology/Democracy. A compilation sample (https://www.youtube.com/watch?v=UmtWUn81d8A&t=51s) of about thirty hours of that program is available on YouTube.



Through all this work, Ariel mentored hundreds of women filmmakers through their productions and produced dozens of works. Among, the later, are Healthcaring (32 mins, 1976), a mainstay at WMM, and the award winning, Women Art Revolution! (83 mins, 2010) by Lynn Hershman, for which she raised a single $100,000 individual contribution. Ariel directed seven of her own films. These range from Sweet Bananas (30 mins, 1973)\[trailer: https://www.youtube.com/watch?v=iHWYON4NQH8&t=29s], a fanumentary about women of different classes to her most recent work, Running Dogs (27 mins, 2020), an experimental weave of footage laid over one continuous shot. Since the mid 2000s under the banner of Media Equity she has been an outspoken advocate for feminist media of all stripes especially clamoring for greater funding. She writes extensively about the intersection of women's media, media rights and funding, most recently for Philanthropy Women (https://philanthropywomen.org/author/aridou/). She is completing a book on 25 contemporary girl community filmmaking workshops in the US with a look back at parallel activities from the 1970s.

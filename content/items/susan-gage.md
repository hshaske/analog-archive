---
title: Susan Gage
itemType: gallery
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
I grew up in the Berkshires in western Massachusetts and always aspired to be an artist.  I graduated from California College of Arts and Crafts in Oakland with a BA in Film Arts (currently renamed California College of the Arts). I initially started as a Textile Arts major. I was inspired to go to the school to study with the director of that program, Trude Guermonprez, an internationally known textile artist who used the medium of weaving to create her artwork. I explored working with wire and other non- traditional materials to create 3 dimensional woven art pieces. 



I later took a class in video and found a new passion. I was fascinated by the immediacy of the medium, and drawn to its potential for feminist social commentary and expression of my views as an empowered young woman. I also studied photography including learning the Zone System developed by Ansel Adams. I changed my major to Film Arts which included both of these disciplines.



I was initially doing abstract art pieces in video, but soon became interested in also doing documentaries. The early 70s were dynamic times to be a young woman. I identified as a feminist and when learning of the movement for women to empower themselves by doing their own pelvic exams, I reached out to the local Feminist Women’s Health Clinic to suggest doing a video on the process. This resulted in my doing the Women’s Self Help documentary with them which was submitted to the International Women’s Film Festival in 1973. The video provides an introductory talk and slide show followed by a demonstration of the self examination process. 



Once graduating from art school,  I needed to make a living and found that I could most readily do this by pursuing a career in photography. I had several jobs in San Francisco. I started out working at Adolph Gasser Photography selling photo supplies and equipment. I also worked at Technicolor Films developing film before starting to work as a commercial photographer mainly doing portrait photography. 

 In my thirties, I decided that I wanted to do more to help others and then completed a master’s degree in Clinical Psychology at JFK University followed by working towards and receiving a license as a  Marriage and Family Therapist. I initially worked in community based mental health as a therapist. Once licensed, I worked as a private practice therapist primarily with trauma survivors. I later became Program Director of Circle of Care/STRIVE Day Treatment Program and School which was an innovative evidence based program for adolescent girls who were considered highly at-risk. The program explored and developed gender specific ways to effectively help adolescent girls use creativity, community, therapeutic support, and behavioral interventions to heal trauma and excel in life. 

I most recently worked as a Program Analyst for the City and County of San Francisco‘s Human Services Agency helping develop and implement a collaborative program to integrate mental health with child welfare practices. 



I have continued doing photography throughout the years. I also have done several installation art pieces  which have combined sculpture and video.(here is a link to one of those pieces: https://youtu.be/Z7RYvNz48MU )   Doing art, being an empowered woman and helping others have been lifelong pursuits.

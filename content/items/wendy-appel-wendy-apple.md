---
title: 'Wendy Appel (Wendy Apple) '
itemType: gallery
poster: /archive/media/wendy-appel-profile-pic.jpg
images:
  - image: /archive/media/wendy-appel-profile-pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
"Wendy Appel (who also spelled her name 'Apple') graduated from New York University's Film School and became one of the first women videographers. After her years with TVTV, she produced and directed a number of documentaries, including The Cutting Edge: The Magic of Movie Editing in 2004. She also taught at the University of Southern California's School of Cinematic Arts and at CalArts. Wendy passed away in 2017."--Allen Rucker

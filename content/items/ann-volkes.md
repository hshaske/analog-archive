---
title: 'Ann Volkes '
itemType: gallery
poster: /archive/media/ann-volkes-street-scene.jpg
images:
  - image: /archive/media/ann-volkes-street-scene.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
I am a native New Yorker. After studying fashion design at Parsons School of Design I worked as a sportswear designer for 3 years. An economic downturn in the fashion industry inspired a shift to working in costume construction and design and positions as a wardrobe mistress on several Off Broadway plays.

During that time in the early 1970s abortion was made legal in New York and California, and I volunteered at a women’s clinic. While there I saw a flyer for a women’s video art exhibition. Portable video production was a new phenomenon. I knew right away that I wanted to get involved in that and if I went to the screening I would find out how to do it.

Susan Milano was the organizer of the event and that is where we first met. Within the next two weeks I was participating in two weekly video workshops. One at the Women’s Interart Center and another at the Sterling Manhattan Cable TV Public Access Center. It was quickly obvious that editing the material produced on the ½” reel-to-reel portapaks was the most interesting and creative part, though extremely challenging and slow.

I worked with Susan Milano on producing and organizing the Women’s Video Festival annually for a number of years, while making my own video art and teaching video workshops. I also conducted panels to judge video art for grants from the New York State Council on the Arts and was co-curator at the Anthology Film Archives video screenings. Eventually I worked at Electronic Arts Intermix video editing and distribution facility.

I was a recipient of grants for video artists from the New York State Council on the Arts and the National Endowment for the Arts.

In 1980 I taught electronic video-editing at the New School graduate program in media and began working at WCBS News as a news editor. In 1988 I transferred to CBS News as an editor for 60 Minutes for ten years and then worked on various other news programs at CBS until retiring in 2008.

---
title: 'Mary Lucier '
itemType: gallery
poster: /archive/media/lucier-mary-headshot-by-cecilia-condit.jpg
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
BIO: 

Mary Lucier has been noted for her contributions to the form of multi-monitor, multi-channel video installation since the early 1970’s. After graduating in sculpture and literature from Brandeis University, she became involved in photography and performance while still living in the Boston area. She traveled extensively with the Sonic Arts Union for several years, collaborating with composers Alvin Lucier and Robert Ashley in concerts throughout the US and Europe. Her work prior to her introduction to video was largely concerned with manipulation of the black and white image through a graphic, performative process as in the Polaroid Image Series, designed to accompany I am sitting in a room by Alvin Lucier. She also produced several live performances with the feminist video collective Red, White Yellow and Black (along with Shigeko Kubota, Cecilia Sandoval and Charlotte Warren) at the original Kitchen in 1972 and '73.

Since 1975 her mixed-media video works such as Dawn Burn, Ohio at Giverny, and Wilderness have consistently explored the theme of landscape as a metaphor for loss and regeneration, while subsequent works such as Noah's Raven, House by the Water, and Floodsongs have examined ecological trauma and transformation in more obliquely narrative modes. She has long been concerned with the drama and power of place as shown in The Plains of Sweet Regret and Wisconsin Arc, where architecture, landscape, and cultural behavior are mutually determinative. 

Lucier's video installations have been shown in major museums and galleries around the world. Many now reside in important collections, among them the Whitney Museum of American Art, NY; the Museum of Modern Art, NY; the Reina Sofia, Madrid; the Stedeljik Museum, Amsterdam; the San Francisco Museum of Modern Art; ZKM, Karlsruhe, Germany; the Milwaukee Art Museum; and the Columbus Museum of Art, Columbus, OH. She has also produced a significant body of single-channel works which have been screened in museums and festivals world-wide.  From the austere black and white experiments of the 1970's to recent studies of Japanese Buddhist ceremonies and Dakota Sioux dances, these works acknowledge the influence of both Avant Garde and documentary practices in American art and cinema.

Lucier has been the recipient of many awards and fellowships, including the National Endowment for the Arts, the John Simon Guggenheim Memorial Foundation, the Rockefeller Foundation, Creative Capital, Anonymous Was a Woman, the Nancy Graves Foundation, USA Artists, the American Film Institute, the Jerome Foundation, the New York State council on the Arts,  and the Japan-US Friendship Commission.  

She lives and works in New York City and Cochecton, NY, where she has established a studio and archive for video art.

---
title: 'Videotape: Woman''s Video Festival '
itemType: review
poster: /archive/media/wvf-shulman.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - {}
pdfembed: filmmakers-newsletter-all
---
"Videotape: Woman's Video Festival"

Rochelle Shulman

_Filmmakers Newsletter_

January, 1974

Vol. 7, no. 3

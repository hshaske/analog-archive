---
title: 'Anda Korsts '
itemType: gallery
poster: /archive/media/anda-korsts-profile-pic.jpg
images:
  - image: /archive/media/anda-korsts-profile-pic.jpg
  - image: /archive/media/fp3305-pxl-camcorder.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
BIO: 

Chicago-based video artist and founder of Videopolis, Chicago's first alternative video space. 

Korsts was an active member of TVTV

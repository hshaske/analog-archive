---
title: 'Keiko Tsuno '
itemType: gallery
poster: /archive/media/keikotsuno-profile-pic.jpg
images:
  - image: /archive/media/keikotsuno-profile-pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Keiko Tsuno has been producing and directing documentaries since 1969. Between 1974 and 1979, together with her husband Jon Alpert, Tsuno co-produced five one-hour documentaries for public television(PBS).

The earliest production, entitled “Cuba: The People”, was the first American documentary inside of Castro’s Cuba. The New York Times selected this work as one of the best television productions in the country that year. 

She and Alpert also produced the documentary called “ Viet Nam: Picking up the pieces” as the first Americans to report after the fall of Saigon and received ‘Du Pont - Columbia Award”. 

In 1980, for “Third Avenue: Only the Strong Survive ”, of which she received the National Emmy Award for Best Editing and the Grand Prix at the Tokyo Video Festival.

In 1985, “Invisible Citizens: Japanese Americans”, which won a Monitor Award's Best Documentary nomination;

In 1993, The Story of Vinh won a CINE Golden Eagle Award and Canal Street: First Stop In America for PBS won a National Education Gold Apple Award and, India Journal (2005) was premiered at a United Nations Film Festival in 2006. 

Originally from Tokyo, she recently published a book in Japan, titled, Changing the World Through Video, recounting her last thirty years of experience in filmmaking. 

Keiko Tsuno is the Co-founder and Co-Director of the Downtown Community Television Center, America's largest and most honored non-profit community media center, which is located in a landmark firehouse in NYC. 



Selected Filmography:

Cuba: The People (1974) PBS

Chinatown: Immigrants in America (1976) PBS                             Reports from Cuba for ABC-TV (From 1976 – 1978)

Healthcare: Your Money or Your Life (1977) PBS

Vietnam: Picking up the Pieces (1978) PBS                                Vietnam – China Border war for NBC-TV(1979)                        Cambodia: After Pol Pot for NBC-TV (1979 -1980)           

Third Avenue: Only the Strong Survive (1980) PBS

Invisible Citizens: Japanese Americans (1985) PBS

The Story of Vinh (1991) PBS

Canal Street: First Stop in America (1999) WNYC/13                      “Cuba and the Cameraman” for Netflex (2018)

---
title: 'Women''s Video Festival Press Release '
itemType: ephemera
poster: /archive/media/wvf-1972-press-release.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
pdfembed: 1972womensvideofestivalatthekitchenadvertisement
---
The Kitchen 

September 14 - 30, 1972

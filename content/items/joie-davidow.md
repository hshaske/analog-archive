---
title: Joie Davidow
itemType: gallery
poster: /archive/media/joie-davidow-profile-pic.png
images:
  - image: /archive/media/joie-davidow-profile-pic.png
  - caption: 'Joie Davidow with Rudi Stern and John Reilly of Global Village '
    image: >-
      /archive/media/joie-davidow-with-rudi-stern-and-john-reilly-of-global-village.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Joie Davidow is  the author of five nationally-published books, an editor and writing coach. She is the author of Marked for Life, a memoir published by Harmony in June 2003, and Infusions of Healing, A Treasury of Mexican-American Herbal Remedies, published by Fireside/Simon & Schuster in October 1999. With Esmeralda Santiago, she is the editor of two anthologies, Las Christmas: Favorite Latino Authors Remember the Holidays, published by Alfred A. Knopf in November 1998, and Las Mamis: Favorite Latino Authors Remember Their Mothers, published by Knopf in April 2000. Her short novel, I Wouldn't Leave Rome to Go to Heaven, was published in 2008. An Unofficial Marriage, a novel about the relentless love of the author Ivan Turgenev for the prima donna Pauline Viardot, will be published by Arcade in the spring of 2021. She is currently working on a novel set in 1749, based on the diary of an 18 year-old girl who was seized at gunpoint from her home in the Rome Ghetto and carried off to a convent where she was relentlessly pressured to convert.

Davidow was a co-founder of the L.A. Weekly, a newspaper which has been an integral part of life in Los Angeles. She served as Vice-President and Director of L.A. Weekly, Inc., and was also an editor of the paper, developing its entertainment section and writing a popular "Style" column.  Subsequently, she founded L.A. Style, the ground-breaking magazine that chronicled the Los Angeles lifestyle, becoming the fastest growing regional magazine in the United States and winning numerous awards. After selling the magazine to American Express Publishing, she launched the award-winning Sí magazine, a national lifestyle publication, in English, catering to the Latino market.

Her television appearances include such programs as the "Oprah Winfrey Show," "CBS This Morning,”  and many others. She received the Liberty Hill Foundation’s Upton Sinclair award, for her role in the founding of L.A. Weekly. She was named one of Publishing’s Most Dynamic Leaders by Folio:The Magazine for Magazine Management. 

In the 1970s she co-founded the video cooperative Global Village in New York City alongside light and video artist Rudi Stern and documentary tapemaker John Reilly. The group took to the streets with the first portable video cameras, documenting political protests and speeches which were not being covered by the three major television networks of the time. She also taught video production at with the Global Village team through the New School.  

She lives in Umbria with her dog Maggie.

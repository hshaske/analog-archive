---
title: 'LoveTapes Collective Gay Pride Screening '
itemType: ephemera
poster: /archive/media/love-flyer-7-21-1973.jpg
images: []
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
pdfembed: love-flyer-7-21-1973
---
LOVE (Lesbians Organized for Video Experience) 

Screening of video tapes of the 1973 Lesbian Feminists in Gay Pride March at the Women's Interart Center 

July 21, 1973

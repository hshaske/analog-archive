---
title: 'Video Women''s Place is in The Kitchen '
itemType: review
poster: /archive/media/wvf-majority-report-.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - {}
pdfembed: majority-report-1972
---
"Video Women's Place is in The Kitchen" 

Cary Herz 

_Majority Report: A Feminist Newspaper Serving the Women of New York_

October 1972

Vol. 2, no. 6

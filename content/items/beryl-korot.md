---
title: 'Beryl Korot '
itemType: gallery
poster: /archive/media/beryl-korot-profile-pic.jpg
images:
  - image: /archive/media/beryl-korot-profile-pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Beryl Korot

born 1945

lives and works in New York City

Throughout her lifelong practice, Beryl Korot has brought the ancient and modern worlds of technology into conversation. An early figure in the history of video art, Korot was first known for her multiple channel video work in which she applied specific structures inherent to loom programming to the programming of multiple channels, constructing non-verbal narratives. Later, she in-vented a visual language based on the grid structure of handwoven canvas. Translating texts into her own language, she illuminated what thought might look like devoid of specific meaning. The sources for much of her work reach back to the technology of the ancient world, whether the technology of the loom or of writing itself. In her work, there is both the visualization of an interior landscape based on language and a spotlight on the intersection between technology and thought.  Most recently her work combines drawings with digital embroidery and large scale hand woven paper tapestries. incorporating printing technology.

Co-founder and co-editor of Radical Software (1970-74), the first publication to
 discuss the technical and formal possibilities of the new medium, Korot was also
 co-editor of Video Art: An Anthology (HBJ, 1976). Her works have been presented at a variety of venues throughout the world: The Kitchen, New York, NY (1975); Leo Castelli Gallery, New York, NY (1977); Documenta 6, Kassel, Germany (1977); the Whitney Museum of American Art, New York, NY (1980, (2002); The Koln Kunstverein (1989); the Carnegie Museum of Art, Pitts-burgh, PA (1990); Historisches Museum, Frankfort (2001); DMZ-2005, South Korea (2005); the Aldrich Contemporary Art Museum (mini-retrospective), Ridgefield, CT (2010); bitforms gallery, New York, NY (2012), the Whitworth Gallery, Manchester,  England (2013); Museum Abteiberg, Monchengladach, Germany (2013); Locks Gallery, Philadelphia, PA (2013); Art Basel, Basel, Switzerland (2014); the Institute of Contemporary Art, Boston, MA (2014); Tate Modern, London, England (2014); Hood Museum, Dartmouth College (2014); Garage Museum of Contemporary Art, Moscow (2014); the Wexner Center for the Arts, Columbus, OH (2015), the new San Fran-cisco MOMA (2016), Santa Fe Thoma Art House (2017), LOOP festival, Barcelona (2017), ZKM 2008/2017-18, Karlsruhe, Germany, and MoMA NYC (2017-18), amongst others.

Two collaborations with composer Steve Reich — The Cave (1993) and Three Tales (2002) — brought video installation art into a theatrical context and toured worldwide.  Both works continue to be performed and were exhibited as video installations at The Whitney Museum (1993); the Carnegie Museum; the Reina Sofia in Madrid; the Kunstverein in Dusseldorf, Germany; and ZKM, Medium Religion, in Karlsruhe, Germany, 2008.

Korot’s work is in both private and public collections including the MOMA, New York, the Kramlich Collection’s New Art Trust (shared by SFMOMA, MOMA NY and Tate Modern) the Thoma Art Foundation, the Hood Museum, the Sol LeWitt collection, amongst others.  A Gug-genheim Fellow (1994), Korot is the recipient of numerous grants including the National Endow-ment for the Arts and Anonymous Was a Woman (2008).  In 2000, she was a Montgomery Fellow at Dartmouth College with Steve Reich and in 2011 she was Artist in Residence at Dartmouth College.

---
title: 'Barbara Buckner '
itemType: gallery
poster: /archive/media/barbara-buckner-profile-pic.jpg
images:
  - image: /archive/media/barbara-buckner-profile-pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Barbara began creating video art in the early 1970s, her first public exhibition was held at the Kitchen in New York City. To create her work, she used electronic image processing tools such as keyers, colorizers, synthesizers and a digital frame buffer to render images which functioned as moving visual ciphers.  From these, she edited sequences which emerged as “structural narratives.” The videos have a rich sense of color as well as a unique, meditative pacing underscored by silence. To Barbara, these types of images seemed to embody a magnetic force that was both invisible and intelligent, and inherently designed to convey spiritual states due to their luminosity.  

In terms of an artistic process, Barbara would begin by recording moving video images, and then brought those library of images into the studio. Using the technology, there was a way to set up image changes or permutations in real time. It was somewhat like witnessing a magical event, because at points during the process one is not physically doing anything. Using voltage, one can combine two or more sets of continuous processes and have a contrapuntal development going on between different picture elements.  One is dealing with the video signal more as a code, being able to manipulate elements of the code separately (gray level, luminance, color, saturation, image mixing, sequencing).

Barbara has exhibited her work at the Museum of Modern Art, Whitney Museum of American Art, and other galleries and museums around the world. Her work Hearts is included in the Museum of Modern Art’s permanent collection.

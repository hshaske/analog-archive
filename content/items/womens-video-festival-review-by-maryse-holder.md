---
title: 'Counter Vulture: Women''s Video Festival '
itemType: review
poster: /archive/media/wvf-oob-1972.png
images: []
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - reviewlink: ''
pdfembed: counterculturereviewofwomensvideofestival1972
---
"Counter Vulture: Women's Video Festival" 

Maryse Holder 

_Off Our Backs _

October 1972

p. 18

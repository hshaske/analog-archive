---
title: 'Antique with Video Ants and Generations of Dinosaurs '
itemType: gallery
poster: /archive/media/lucier-mary-antique-b-w-photo.jpg
images:
  - image: /archive/media/lucier-mary-antique-b-w-photo.jpg
  - image: /archive/media/lucier-mary-antique-color-phot.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Mary Lucier, Antique with Video Ants and Generations of Dinosaurs

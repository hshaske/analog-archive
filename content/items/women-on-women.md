---
title: Women on Women
itemType: video
poster: /archive/media/vsw-women-profile-pic.png
images:
  - imageUpload: /archive/media/vsw-women-profile-pic.png
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
annotations:
  - files:
      - {}
    images:
      - {}
mediaUrl: 'https://vimeo.com/345792747'
---
Portable Channel; Women's Television Workshop 

1973

---
title: 'Barbara Buckner - Sample '
itemType: video
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
annotations:
  - files:
      - {}
    images:
      - {}
mediaUrl: >-
  <iframe src="https://player.vimeo.com/video/107036845" width="640"
  height="480" frameborder="0" allow="autoplay; fullscreen"
  allowfullscreen></iframe> <p><a href="https://vimeo.com/107036845">Work Sample
  - Barbara Buckner</a> from <a href="https://vimeo.com/user28756273">Barbara
  Buckner</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
---


---
title: 'Video Playback: Annual video-tape extravaganza stresses women''s point of view '
itemType: review
poster: /archive/media/video-playback.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - reviewlink: ''
pdfembed: 1973videoplaybackstresseswomensviews/mode/2up
---
"Video Playback: Annual video-tape extravaganza stresses women's point of view"

Roy Pinney

_Popular Photography _

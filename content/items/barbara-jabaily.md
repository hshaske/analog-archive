---
title: 'Barbara Jabaily '
itemType: gallery
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
See LoveTapes Collective 



I started as a street photographer in the lower east side of New York.  I studied photography at School of Visual Arts in the early 70s and graduated in the first accredited year. I learned from some pretty amazing photography legends and was truly inspired by my surroundings.  I did some post graduate work in film criticism and theory at New York University and spent several years on staff at the Women’s Interart Center in New York. During this time, I witnessed the rise of social change movements – anti-war, women’s rights and lesbian & gay rights. 

In 1972 I got I got involved with courses in port-a-pak video production at the Video Access center and that started my career as a activist journalist.  I got  involved with the L.O.V.E. group, then called Lesbians Organized for Video Experience (now is the LoveTapesCollective) to record events and bring forward images of women and lesbians that was absent from mainstream media. LoveTapesCollective is still active in archiving and licensing our early work from 1973 through 1976. After graduating from College I got a job at Women’s Interart Center in the brand new filmmaking workshops and learned the art of independent filmmaking in 16mm. I worked in 16mm film until I relocated to Colorado in 1979. 

I started my career at KBDI Channel 12 as a videographer and editor and where I worked my way up to Producer/Director.  During that time I created my own social commentary documentaries, won some awards and at the same time I worked on many other people’s work as videographer and editor. Additionally, I took over producing Teletunes, one of the country’s first music video programs as well as creating Colorado Inside Out a local journalistic pundit roundtable which recently celebrated its 25th anniversary and is still broadcasting every Friday night.  

Since moving to Pueblo in 2000, I feel as though I have come full circle and now back to my roots with photography. I continue to create video projects for festivals, and shoot and edit video for local activist groups and for fun. But most important to my well being is my love of nature and being out on the prairie or by the Lake or in the wilderness. I love photographing the beauty, the atmosphere and sometimes the starkness of landscapes and empty spaces. This pandemic has brought about more change and the way I view my life and the world and it shows in my photography where I focus on found objects and beauty and also put together scenes from around my private spaces.

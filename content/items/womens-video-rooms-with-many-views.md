---
title: 'Women''s Video: Rooms with Many Views'
itemType: review
poster: /archive/media/wvf-majority-report-75.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - {}
pdfembed: majority-report-1975
---
"Women's Video: Rooms with Many Views"

Lyle Ruppenthal

Majority Report

May 3, 1975

Vol. 4, no. 25

---
title: Off Our Backs
itemType: ephemera
images:
  - caption: Pg1
    image: >-
      https://archive.org/download/offourbacksfirstinternationalfestivalofwomensfilmsinnyc1972/Off%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_jp2.zip/Off%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_jp2%2FOff%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_0001.jp2&ext=jpg
  - caption: Pg2
    image: >-
      https://archive.org/download/offourbacksfirstinternationalfestivalofwomensfilmsinnyc1972/Off%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_jp2.zip/Off%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_jp2%2FOff%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_0002.jp2&ext=jpg
  - caption: Pg3
    image: >-
      https://archive.org/download/offourbacksfirstinternationalfestivalofwomensfilmsinnyc1972/Off%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_jp2.zip/Off%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_jp2%2FOff%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972_0003.jp2&ext=jpg
collections:
  - {}
resources: []
keywords: []
related:
  - items:
      - {}
ephemerafiles:
  - description: adfa
---
This is the Off Our Backs Pamphlet.

You can download the PDF from Internet Archive [here](https://archive.org/download/offourbacksfirstinternationalfestivalofwomensfilmsinnyc1972/Off%20Our%20Backs%20-%20First%20International%20Festival%20of%20Women%27s%20Films%20in%20NYC%20-%201972.pdf).

---
title: 'Louise Denver '
itemType: gallery
poster: /archive/media/louise-denver-profile-pic.jpg
images:
  - image: /archive/media/louise-denver-profile-pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
A former documentary film maker and international journalist, Louise Denver was based in the Middle East, Northern Ireland, London and New York and subsequently became a corporate communications leader in Australia for close to 25 years. 

She was lucky enough to lead a pioneering internal communications function at a Big Four bank in Australia, as well as develop both internal and the communications side of social at two of the world’s Big 4 professional services firms. At Deloitte she is currently responsible for corporate affairs and communications for two industry groups - Financial Services and Technology, Media and Telecommunications - a service line - Risk Advisory - and as a senior member of the Corporate Affairs and Communications team looks after some internal and regional communications. A seasoned wordsmith and senior writer Louise also helps develop some of Deloitte’s Australian thought leadership and points of view.

---
title: 'Ulrike Rosenbach '
itemType: gallery
poster: /archive/media/ulrike-profile-pic.png
images:
  - image: >-
      https://archive.org/download/ulrike-profile-pic/Ulrike%20-%20profile%20pic.png
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Born in 1943 Bad Salzdetfurth, Germany.

From 1964 to 1972 Rosenbach studied sculpture at the Kunstakademie Dusseldorf; 1970; she became a master student of Prof. Joseph Beuys. 

In 1971 Rosenbach produced her first videoworks and first performance-works in 1972. Since then, media works with video/audio, photography and performance as well as media- installations were her professional tools.

In the mid-seventies of the last century, her artwork was highly interesting as dealing with topics like traditional role models of women. This concept then helped to formulate an identity for a female esthetic theory from a feminist perspective (Lucy Lippard “From the Center”).  1975 she founded a workshop “school for feminist creativity” in Köln and called herself a feminist artist She then performed a series of spectacular media performances at various international art-institutes  and festival such as the Biennale des Jeunes at Paris 1975 (“Don’t believe , I am a Amazone”, media performance)

Ulrike Rosenbach became a wellknown international media-artist of her generation. In 1977 her installation- and performance - work was exhibited at documenta 6 in Kassel/germany and 1987 at documenta 8. For her work Rosenbach was rewarded with various art-awards.

After 1985 Rosenbach`s artwork concentrated on media-installation, media-sculpture, drawing and mixed media/photography. 

From 1989 – 2007 she held a professorship at the college of Hochschule der Bildenden Künste Saar HBKsaar in germany. 

In this frame, she also was a curator of a series of art- projects for and with her students. She also, with her students, then initiated a net-archive for women-media-artists: www.bild-rausch.com

Today she lives and works as a freelance artist in the area of Köln/Colon in Germany - developing and showing her artwork at international galleries and institutions such as Priska Pasquer gallery Köln. Her work is part of various international collections.

www.ulrike-rosenbach.com

---
title: 'Jackie Cassen '
itemType: gallery
poster: /archive/media/jackie-cassen-profile-pic-no-rights-.jpg
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
BIO: 

Pioneer in video and light art, Cassen was most well-known for her lights shows at the Electric Circus nightclub in NY.

Collaborated with Nam June Paik on "Experiment," 1971, NET-TV Workshop, NY.

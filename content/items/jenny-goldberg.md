---
title: Jenny Goldberg
itemType: gallery
poster: /archive/media/jenny-goldberg_profile-pic-1.jpg
images:
  - image: /archive/media/jenny-goldberg_profile-pic-1.jpg
  - caption: ''
    image: /archive/media/jenny-goldberg_profile-pic-2.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Jenny Goldberg grew up in the rough, rioted North End of Hartford, Connecticut, and ran away to NYC as a teen.  She got her first job selling postcards at the Guggenheim Museum, and then worked her way through New York University Institute of Film and Television, taking jobs in the film industry to pay for her education.  She photographed and edited a series of videos about children with special needs in the public schools, which was produced and distributed by Model Cities and Project Head Start.  At the Center for Public Interest, she shot and edited a series of videotapes called “The Elders”, that was about and for seniors.  

During the seven years it took her to earn her Bachelor of Fine Arts degree, she got to know other women like herself, who were survivors of rape, and that was the genesis of the compelling 1972 documentary video, “The Rape Tape”, presented in the first Women’s Video Festival that same year.

Each of the four women shared how they were affected by their interactions with the authorities, courts, parents, doctors, and the rapists, and spoke candidly of the violence, fear, anger, and shame.  The tape was subsequently used in public health training programs by hospitals, the police, and schools.  

While attending New York University, she lived in a rundown tenement on East 7th Street and made an organizing videotape called “Rent Strike” about tenants living in Lower East Side tenements who had stopped paying their rent due to intolerable living conditions that their landlords refused to repair.  “Rent Strike”, which documented the conditions, was shown in court to defend the tenants’ rights to withhold rent without threat of eviction and to force their landlords to make essential repairs.  They won their case! 

Using her skills as a producer, camerawoman, and editor, she worked with two others to make a documentary film about tenants’ rights and rent strikes in New York City called “The Home Front,” a 16mm film distributed by Third World Newsreel.  Ironically, one reviewer noted that, “The feminist impact of this film is when one realizes that all of the organizers in the film are women pushed against the wall by systems.”  Jenny, herself, was no stranger to that circumstance.

Although she had steeped herself in urban concerns, she had a strong sense of place, having snuck off to a commune in southern Vermont and to New Jersey’s Palisades whenever she could get away from her many jobs and schooling.  In 1974, having read about Big Sur in Jack Kerouac’s books, she moved to California where she beheld the Pacific Ocean flowing into and out of the tidepools at Big Sur, and began her life again, looking for work and exploring the San Francisco Bay Area where she lived.

The first job she landed was editing a documentary video, “Pioneer Aviation”, for the Oakland Museum.  Subsequently she auditioned for and was hired as a broadcast engineer at KQED-TV, working mostly as a camerawoman and editor.  While there, she received an independent grant through Bay Area Video Coalition to produce one of the six segments of their series, WESTERN EXPOSURE, which aired on PBS stations.  The result was “Wheels Go Round”, a portrait of a wheelchair-bound man with cerebral palsy who was an activist for the disabled.  It was heralded as “one of the most disturbing and provocative tapes in the series.”  When Neil Jacobsen, the subject of the piece, learned she planned to hire an interviewer, he urged her to do it herself, and his judgment proved to be prescient.  Her voice and her words open the program,  “I always thought if I were disabled I would want to commit suicide…I figured if any of these disabled people are actually happy, then they must have a secret to life that I don’t have.”

She worked at KQED until October, 1989, when the big Loma Prieta earthquake, with its fire, smoke and damage to her home, propelled her to get away to clean air in the mountains of New Mexico.  Once there, she felt the beauty of the high-desert landscape, and believing that we become what we behold, she realized that she couldn’t leave this extraordinary part of the world.  She has been there ever since, though still misses the expanded culture of San Francisco.

  

Goodbye to the City I Love

\    San Francisco, 1989

On these headland rocks across your strait 

I feel your tremors in the night

watch you in your burning dress 

the orange rouge of fire 

drifting from your face



\    I danced with you

as you waved your scarf of raucous gulls

and twirled in heels of liquid sand

your veil of fog lifting in the wind 

your golden eye 

open to this world of rolling ground, shifting light





Jenny lives in a small adobe house on a windy mesa within view of stunning mountains and the open sky. She writes, performs and publishes her poetry, works as a gardener, and adopts dogs in need of a loving home. Their daily walks on the open mesa provide inspiration for her landscape photos.



Ragged



My lover gone, I marry the old

hot water heater whistle, the clang of woodstove

cooling, mud falling inside jacal walls



lie down to the scurrying of mice, winter

with no one to blame



rise to the scent of snow melt and mist shred, climb

frost scars and tremors, reach for



devious roots, loose fragments of rock

the fellfield



Scorpionweed stings my lips, I kiss

elfinwood, touch timberline, naked tundra



steep slopes of talus and scree

I climb up, fall up, crawl



the windscoured ridge, chase

euphoria up and down the false summits, breathe



the animal pleasure of mounting alone

the last steep pile, press myself



to the highest rock and marry the ragged peak





Goodbye to the City I Love © 2020 Jenny Goldberg.  Ragged © 2010 Jenny Goldberg. Published in Adobe Walls, an Anthology of New Mexico poetry No. 1, 2010

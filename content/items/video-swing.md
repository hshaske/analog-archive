---
title: 'Video Swing '
itemType: gallery
poster: /archive/media/video-swing.jpg
images:
  - image: /archive/media/video-swing.jpg
  - image: /archive/media/video-swing-in-action.jpg
  - image: /archive/media/cahiers-du-cinema-video-swing_edit.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Susan Milano, Video Swing, 1974

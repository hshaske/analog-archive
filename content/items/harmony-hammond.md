---
title: 'Harmony Hammond '
itemType: gallery
poster: /archive/media/harmony-hammond-profile-pic.jpg
images:
  - image: /archive/media/harmony-hammond-profile-pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Photo: Heresies Film Project; A.I.R. Gallery

Harmony Hammond is an artist, art writer and independent curator. A leading figure in the development of the feminist art movement in New York in the early 1970s, she was a co-founder of A.I.R., the first women’s cooperative art gallery in New York (1972) and Heresies: A Feminist Publication on Art & Politics (1976). Since 1984, Hammond has lived and worked in northern New Mexico, teaching at the University of Arizona, Tucson from 1989–2006. Hammond’s earliest feminist work combined gender politics with post-minimal concerns of materials and process, frequently occupying a space between painting and sculpture – a focus that continues to this day.

Her near-monochrome paintings of the last two decades participate in the narrative of modernist abstraction at the same time they insist upon an oppositional discourse of feminist and queer content. Often referred to as social abstraction, the paintings which include rough burlap, straps, grommets, and rope, along with Hammonds signature layers of thick paint, engage formal strategies and material metaphors suggesting connection, restraint, agency and voice - a disruption of utopian egalitarian order, but also the possibility of holding together, of healing. A survivor aesthetic.

Hammond’s work is represented by Alexander Gray Associates, NYC, where she has had three solo exhibitions (2013, 2016 and 2018). Her artwork was included in major exhibitions such as "Painting 2.0: Expression in the Information Age" (2015-2016); "Wack! Art and the Feminist Revolution" (2007) and "High Times/Hard Times, New York Painting 1967–1975" (2006-2007). The Aldrich Contemporary Art Museum in Ridgefield, CT is presenting “Harmony Hammond: Material Witness, Fifty Years of Art”, a survey exhibition that opened March 3rd and remains on view through September 15, 2019. The exhibition will be accompanied by a fully illustrated catalogue, the first monograph on Hammond’s work.

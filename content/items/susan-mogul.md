---
title: 'Susan Mogul '
itemType: gallery
poster: /archive/media/susan-mogul-profilepic.jpg
images:
  - image: >-
      https://archive.org/download/susan-mogul-profilepic/Susan%20Mogul%20-%20profilepic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Having been involved with video since the early 1970s, Mogul is a pioneer of the medium. Initially producing an important series of humorous and staunchly feminist performance videos, her practice quickly expanded to more complicated and experimental forms of narrative, including feature length films. 

Mogul has received grants and commissions including: a Guggenheim Fellowship, ITVS commission, National Endowment for the Arts Fellowship, Getty Trust Fellowship, and Center for Cultural Innovation project grants. Women of Vision: Histories in Feminist Film and Video, devotes a chapter to Mogul's work. 

Mogul’s video/film retrospective was presented at “Visions du Reel” Film Festival in Switzerland in 2009. Driving Men (2008), a feature length personal documentary, screened in prestigious documentary festivals in Japan, Italy, Portugal, Switzerland, India, and Taiwan. 

Mogul was the keynote speaker at a conference in Zurich on film and autobiography, lectured in the series “Critical Issues in Contemporary Art” at the Henry Gallery in Seattle, and presented “Comedy as a Back Up”, a lecture at the Wattis Center for Contemporary Art in San Francisco. 

In 2019 “Mom’s Move” (2018, 25 min.) screened at the National Gallery of Art in Washington DC, and, her one-woman installation, “Less is Never More”, premiered at As-Is gallery in Los Angeles. “Less” received a full-page review in the LA Times. X-TRA Contemporary Art Quarterly will feature a major essay on both projects in June 2020. 

Mogul’s video art was selected for two distinct historical exhibitions at major art museums in Poland and Austria that opened in 2020: “Early Video Art” at the Zacheta National Gallery of Art in Warsaw; and, “The Early Years of CalArts” at the Kunsthaus Graz in Austria. Additionally, the curators of both museums invited Mogul to present lectures and screenings in March and April, in conjunction with their exhibitions. However these invitations were postponed due to Covid 19. 

NATURE OF WORK 

No matter how hard I fight the impulse, I am compelled to make art from my life. In 1973 I moved to Los Angeles, 3,000 miles from home, to be part of the feminist art movement. Ever since, my work has confronted traditional female roles through any means necessary – video art, performance, photography, and personal documentaries. I mix and blur genres to create dramatic and poetic narratives out the everyday. 

For the last thirty years filmmaking has been my primary medium. My films often reflect and respond to the wide array of socio-cultural groups I belong to and/or encounter: my Los Angeles neighborhood, childless women, secular/cultural Jews, my family, and the feminist art movement. In that regard, my humorous and poignant film “Driving Men”, is one example of the fact that my work crosses boundaries. “Driving Men”, was not only selected for international documentary film festivals, but women’s and Jewish film festivals as well.

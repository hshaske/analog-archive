---
title: 'Nina Sobell '
itemType: gallery
poster: /archive/media/nina-sobell-profile-pic.jpg
images:
  - image: >-
      https://archive.org/download/nina-sobell-profile-pic/Nina%20Sobell%20-%20profile%20pic.JPG
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Nina Sobell, EEG: Video Telemetry Environment, 1975

Contemporary Art Museum (CAM) Houston, TX

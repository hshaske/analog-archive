---
title: 'Susan Milano '
itemType: gallery
poster: /archive/media/susan-milano-profile-pic.jpg
images:
  - image: /archive/media/susan-milano-profile-pic.jpg
collections: 
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---


INTERVIEW: 

When did you become interested in video?  

For me, it began in late 1970, when I saw an ad in the Village Voice advertising video shows/screenings at a place called Global Village, with the names John Reilly and Rudi Stern in the text.  I had known a John Reilly since I was in high school, and had been participating in theatre productions at Seton Hall University where he was a student.  He had a radio program on campus there and we had become friends.  He sometimes took me to off-Bway productions and did photo shoots of me, and we had kept in touch on and off over the years.  But I didn’t know if this John Reilly was the same person or not.  So, I called and discovered that in fact it was.  I said what is video?  He said come to a show I think you’ll like it.  I did, and he was right. 

I had been a child of television.  Growing up, when I was home from boarding school on the weekends, the television was my companion.  When I saw the first show at Global Village it was exciting to see programming that was documenting the counter culture scene, of which I was now a part….living a hippie lifestyle in a loft on the Lower East Side, involved in the psychedelic revolution, studying music at Henry Street Music School, hanging out with wannabe musicians…..  While I wasn’t an intellectual, I strongly identified with the beat generation and the style that went with it.  I had been working at a small think tank ad agency whose mission was to invent/create/launch new products that would capture the public’s imagination.  It was a small group and I was the admin assistant to the president.  Over the time that I had been there I had enjoyed the opportunity of seeing the entire process of such development and it was fun and exciting, but when John suggested I leave my job and come help him at “Global”…..I said yes.

I was living very cheaply and I became his right hand.  The first shoot he took me on was the tape he was making of AJ Weberman, a frustrated journalist who achieved a certain notoriety stealing Bob Dylan’s garbage and then writing articles for the Village Voice about it.  It was a cold winter day and I was lying down (out of frame) on the sidewalk taking sound as AJ railed about the fact that the household had stopped putting their garbage out.  At a certain point, the housekeeper came out and the two of them verbally sparred with each other….John shot it and at the end he pulled back and revealed me flat on the ground in my maxi coat holding the mic.  I took the whole scene in…we’re in front of Bob Dylan’s house;  the family is hiding their garbage…wow….THIS IS FOR ME!  The Ballad of AJ Weberman can be seen for free at     https://mediaburn.org/video/ballad-of-a-j-weberman-2/	(Note..no credit for me, but I’m there at the end).

Did you consider your work aligned with video art or video activism, or both? 

Different things at different times.  Coming up as I did at Global Village I thought more in terms of video activism…I was always interested in doing things that women didn’t usually do….wanted to be a blues and rock n’ roll singer long before white women did such things.  Video was exciting and I felt that as a woman I had an advantage when I was walking up to strangers on the street and engaging them in interviews.  They would never perceive me as a threat…after all, I was only a woman.  It wasn’t until I took a workshop with Shirley Clarke and she invited me to become a member of her TP Videospace Troupe, that I started to think of video more in terms of art.  And I think that the work I did as an artist was much stronger than what I’d done before.

Which video artists influenced you?  

Without skipping a beat….Shirley Clarke, although I think it’s fair to say that most people don’t think of her in those terms.  Everyone has so much respect and admiration for her films….rightly so.  But people have really overlooked her prescient and dynamic work with video.  No one was doing the kind of experimenting that she was.

What was the video scene like in the 1970s? How did it change by the 1980s?   

I think my story about AJ Weberman captures one aspect of its playfulness in the 70’s.  But it was also serious.  I remember John Reilly sending me up to Harlem by myself to shoot a demonstration that took place there after Attica.  I got up on a table top and started panning the crowd, when a woman came over and asked me to get down.  I mentioned that we (Global Village) were doing some work with the Panthers, but that didn’t have any effect and I just, wisely, got down. Nothing happened and I realized how naïve I was.  It was a learning experience.  The video community was a small one and we all knew each other.  I met the Vasulkas right away and Nam June and Shigeko and many others.  There would be communal events anti-nuclear causes, charity events, Charlotte Moorman’s Avant Garde Festival (where I met John Lennon).  I traveled to D.C. with Reilly and we videotaped some hearing that took place there about TV and cable (I think it was)….Overall, there was a palpable tension between “artists” who had taken up video as a medium and “activists” who were using it very differently. 

What role did feminist politics play in your thinking at the time? In your work?  

My father was thrifty and consequently, at an early age I inherited my brother’s bike, which was a little big for me but my uncle taught me how to mount and dismount in style.  So I took great pride in the fact that I was often just as good if not better than the boys on the block.  None of them would pick up snakes (I’m talking “garden snakes”).  But my taste in clothing tended to be leaning toward sexy but 16 going on 25.  I longed to sing the blues at a time when white women doing so were rarer than hens’ teeth.  What I’m trying to say is that didactically I had no politics, but in truth, I was a feminist waiting to be born.  When I got into video I wasn’t on a mission, I was just enjoying what I was doing and like other women in video at the time, we just did what we had to do. 

How did the idea for the Women’s Video Festival develop?  

Please see the piece I wrote in the 1976 catalog (attached). I think that kind of answers this but if you have further questions let me know.

What goals did you have for the festival & how did you envision its role in the NY art world? 

The original goals were about getting women to come forward and present their work and know that there was a place where such interest existed.  That was initially why we did no judging.  It resulted in very uneven quality but we wanted to get the word out and we succeeded.  Within two years we were getting a substantial flow of entries and we started an informal jury process.  I never personally thought of the festival as having a “role” in the art world until after the Shirley Clarke workshop, when I was turned on to video as process, as sculpture, as environment….not just another way to make films.  I’ll never forget the 1975 show…the year when we first created viewing environments, and featured sculptures and toys and interactive pieces…I’m pretty sure it was David Loxton from WNET who came to the show…when he walked in he turned to someone who was with him and he said….”Did you see what they did here?”  It was in a somewhat astonished tone of voice.     

Is there any portion of the festivals that you would like to reprise or change? 

I would love for people to experience and know that it was so much more than just videotapes.

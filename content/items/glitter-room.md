---
title: 'Glitter Room '
itemType: gallery
poster: /archive/media/glitter-room-overhead.jpg
images:
  - image: /archive/media/glitter-room-overhead.jpg
  - image: /archive/media/glitter-room-cloud-detail.jpg
  - image: /archive/media/glitter-room-wfolks-int.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---


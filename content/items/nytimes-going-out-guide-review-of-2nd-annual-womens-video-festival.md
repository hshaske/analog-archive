---
title: 'Going Out Guide: Ms. Message '
itemType: review
poster: /archive/media/nyt-going-out-guide-.png
images: []
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - reviewlink: >-
      https://archive.org/details/1973nytimesgoingoutguidefeaturing2ndannualnywvf
pdfembed: 1973nytimesgoingoutguidefeaturing2ndannualnywvf
---
"Going Out Guide: Ms. Message"

Richard F. Shephard 

_New York Times_

Oct. 5, 1973

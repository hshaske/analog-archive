---
title: 'Rochelle Schulman '
itemType: gallery
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Rochelle Schulman was one of the founders of the Women’s Video Project, a feminist media group that came out of the long-defunct Video Access Center (“VAC”) in New York City (1972-’74). The VAC was founded in 1972 by George Stoney and Red Burns (two of the earliest activists involved in the national fight for public access) and with a cache of video equipment provided by the local cable company, provided access and instruction to this new technology. Schulman was a staff member of the Center, where she was an instructor and producer. Her life was tragically cut short in early 1974 when she died while on a hiking trip in California.

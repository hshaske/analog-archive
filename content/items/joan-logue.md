---
title: 'Joan Logue '
itemType: gallery
poster: /archive/media/joan-logue-profile-pic_1.jpg
images:
  - caption: >-
      Joan Logue, Neighborhood Portraits:  Rue des Ecouffes,  Paris France,
      1983 


      Photo by Antoine Monorys 
    image: /archive/media/joan-logue-profile-pic_1.jpg
  - caption: |-
      Joan Logue, Fifth floor apartment on Rue des Ecouffes, Paris France, 1982.
      Photo by Alan Kleinberg 
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
How did I become interested in video and my interest in feminism.

Joan Logue, videoportrait@gmail.com



My work in video started in 1969 while I was working at the American Film Institute.  I was hired to be AFI’s official documentary portrait photographer, which I loved. But one day I discovered a video Portapac in there storage room and with the help of a friend who lived in Santa Barbara, showed me how it worked. What I loved about video was the immediacy of the image – no lab, no other person had to be involved, it was like painting, just me and my Sony 2400!  Without much thought, I started AFI’s video production department!

Meanwhile in Venice California where I was living, there was a video community forming, David Greenberg had formed an organization called Environmental Communications and John Hunt ran the Tuesday night’s video programs. We met every Tuesday night, artists brought reel-to-reel videos and John Hunt the video DJ (as he called himself) showed anyone and everyone who wanted to screen his/her video.  

In 1971 I was offered and took a job at California Institute of the Arts teaching video in the film school (thanks to Pat O’Neill who proposed my name for this position.) While teaching, I met an anthropologist who invited me to go with him and his students to Sucromu Liberia.

He asked me to run video workshops in this small community, located eight hours “up country” from Monrovia. What he didn’t tell me there was no electricity and video needs electricity!  My only demand or stipulation was that I wanted as many women as men in my workshops and he agreed. What I learned there was interesting;  Sucromu was a polygamist society so women lived and worked together as a team (some men had four or five wives.) Teaching the women was easy because even though I didn’t speak Kpelle, they understood and were receptive to another woman showing them something new. The men behaved and worked differently because normally they didn’t speak to women so they didn’t listen or understand the process like the women did.  In the end the women’s work was far superior than the men, but when the women realized they did better - they all quit except one. In a polygamist society women want to be part of a larger family, because more wives mean sharing the work on the farms, cooking, cleaning and helping each other with the children. 

So back to the story; when I returned back to Cal Arts, art faculty members artists Judy Chicago and Mariam Shapiro, were in the throws of creating the Womanhouse, the first independent space for women artists located Downtown Los Angeles. Womanhouse made it clear what I and other women artist were facing in the art world – women were not thought of as serious artists, that club was private, men only!  It was at this time I joined a women’s group called Consciousness Raising) with Beverly O’Neill and Betye Saa, that too was an inspiration, hearing from women artists and art administrators about what was really happening or not happening in the art world.

When I was very young I remember my mother telling me the story of her working at the bank, but when she got married she had to hide her marriage because women were never allowed to work at the bank and be married!  This was my first reality check and introduction to how women were treated. While growing up I watched my mother and father worked together, and my mother seemed to do her work the way she wanted; she designed and built homes and I watched her deal with architects and contractors, never hearing them tell her what she wanted.  My fathers always said to do what you want to do and not depend on someone else to do it for you!!! They were both progressive without me knowing it!

When I got out of school (a painter and photographer) I too ran into obstacles while looking for work; I tried to join the cameraman’s union (the name should have given me a clue) and was laughed out of the office. I tired to get a job editing at CBS but was told they already had a woman in the department!  I was one of three women at the American Film Institute, given free range to shoot portraits of all the film directors (men of course) that came to the institute to speak. I had set up my own darkroom at the institute, so I developed my film, made large prints and was allowed to hang my portraits on the walls of AFI plus running their new video program!  However that said, I was fired because I made “to much noise” meaning, I was asking for lights, more equipment needed to run there video program (Later AFI’s Jan Haag was sent to rehire me but it was too late!)  At Cal Arts, I was the only woman faculty member in the Film School and later found out that all the men were being paid three times more than me.  In New York my first job was at Chase Manhattan Bank – I had just run out of unemployment from Cal Arts and by chance met an old student from Cal Arts on the street who told me about this job at Chase. My interview was with Meg Gottenmiller, who had just laid off all the people in their video department and she asked me what I did. I said I was a video artist and to my surprise she said great, and I was hired!

 A Women’s Festival was happening in New York and I was invited, it was my first all women’s art event!  I was in living in Venice California and New York was a dream away for me in 1974.  I remember Charlotte Moorman calling and invited me to join the festival, I was thrilled and also shy about showing my work, it seemed to me I wasn’t ready but she convinced me to show my work, so I sent the tape!  Actually it was thanks to this festival I ended up meeting Shigeko Kubota when she came to the Long Beach Museum, again thanks to two women from Some Serious Business (Susan Martin and Nancy Drew) who brought Shigeko to my Venice Studio; I made a video portrait of Shigeko and our friendship began once I moved to New York.

 In 1977 I moved to New York, took a cab from the airport to my new sublet on Crosby Street, a loft that Shirley Clark’s daughter owned. I felt at home immediately, women were on the move in New York and women were working in video art!  I remember a cab driver saying to me, what do you do? Replying, I’m a video artist, I make video portraits!  The cabby didn’t blink an eyelash and said “that’s great!”  I knew I was home, New York New York!  

One day on Spring Street I ran into Shigeko Kubota who was surprised to see me in New York and invited me to dinner.  Shigeko a pioneer video artist, smart, inventive, and she ran Anthology Film Archive video program and invited me to her Sunday night video screenings. It was thanks to Shigeko I met the video community of New York City.

My first exhibition in New York City was in 1978 at the Holly Solomon Gallery (thanks to Anthology’s equipment and again to Shigeko Kubota’s help.) I didn’t know Holly Solomon but I walked into her gallery and showed her my video portraits of California Artists and Holly agreed to let me install the video portraits on a monitor in her front gallery window.  She was courageous and she loved Shirley Clark too (Shirley Clark was one of my video portraits and I think that is why Holly said yes.) But it was in the dead of winter and I would go at midnight and turn off the video deck and monitor and first thing in the morning go back and reset the video deck to repeat and turn on the monitor!  One night around midnight I walked over to Holly Solomon’s Gallery to watch my portraits play to an empty snowy street but to my surprise, a homeless man who I always saw in the subway and who I gave money to was standing in front of the window.  We talked and he said he felt happy to watch the video. I told him I made these portraits.  He then gave me a locket on a chain from his pocket, a portrait of a woman from the 18th century and I still have it!   And thus began my life in New York City.

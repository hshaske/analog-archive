---
title: '1973 Kitchen Calendar '
itemType: ephemera
poster: /archive/media/wvf-1973-kitchen-calendar-.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
pdfembed: 1973kitcheneventscalendarsusanmilanosdialogueonvideowithvideo1973.03
---
Featuring: 

"Women Back in the Kitchen: Video Tape Benefit for the Women's Interart Center"

"Dialogue on Video with Video: Elenore Lester, Susan Milano, & The Vasulkas"

---
title: 'Nancy Cain '
itemType: gallery
poster: /archive/media/nancy-cain-profile-pic-1.jpg
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
NANCY CAIN began playing with video as a member of Videofreex, the radical video collective in New York that shot footage of the Woodstock Festival and the Chicago Eight. She worked on the first video pilot ever shot for network television with the Videofreex at CBS in 1969, and ran an offbeat weekly video show at the Videofreex loft in Soho. She cofounded Lanesville TV—known as “Probably America’s Smallest TV Station.” The pirate broadcasts were made possible by a transmitter donated by Yippie activist Abbie Hoffman. Along with TVTV, she defined the video documentary movement of the 70s, known as “guerrilla television.”

Cain was a co-creator and producer of The ’90s, a weekly hour-long alternative show for PBS, which the New York Post called “refreshingly irreverent, opinionated and outlandish.” She was the co-creator and producer of CamNet The Camcorder Network, America’s first all-camcorder channel.

---
title: '"Transsexuals" review in Radical Software '
itemType: review
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - reviewlink: >-
      https://archive.org/details/1972radicalsoftwaresusanmilanotranssexualsvdepidemic1973
pdfembed: 1972radicalsoftwaresusanmilanotranssexualsvdepidemic1973
---
Write-up of 1st National Video Festival sponsored by the Minneapolis College of Art & Design in Radical Software.

Includes reference to Susan Milano's "Transsexuals." 

"Produced as a group project at Global Village, represented by John Reilly, New York."

---
title: 'Tattoo '
itemType: video
poster: /archive/media/tattoo-thumbnail.jpg
images:
  - image: ''
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
annotations:
  - files:
      - {}
    images:
      - {}
mediaUrl: 'https://vimeo.com/396802231/74b0b130b9'
---
Video by Susan Milano

[In MoMA's collections](https://www.moma.org/collection/works/118295?sov_referrer=artist&artist_id=34918&page=1)

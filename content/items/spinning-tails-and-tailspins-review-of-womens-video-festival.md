---
title: 'Women on Tape: Spinning Tails and Tailspins '
itemType: review
poster: /archive/media/wvf-spinning-tales.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
reviewfiles:
  - reviewlink: 'https://archive.org/details/villagevoicespinningtailsandtailspins1972'
pdfembed: villagevoicespinningtailsandtailspins1972
---
"Women on Tape: Spinning Tails and Tailspins" 

Robin Reisig

_The Village Voice_

October 5, 1972 

Includes references to Joanne Kyger's "Descartes," Susan Milano's "Tattoo," Steina Vasulka's "Let It Be," Under One Roof Video Collective's "The Rape Tape," and Rita Moreira and Rita Pontes's Lesbian Mothers.'

---
title: The Dying Swan
itemType: video
poster: /archive/media/swan.jpg
images:
  - {}
collections: []
resources: []
keywords: []
related:
  - items:
      - {}
annotations:
  - body: This is an annotation
    end: '00:00:35'
    files: []
    images: []
    start: '00:00:30'
mediaUrl: >-
  https://archive.org/download/mcdonaldlauriethedyingswan/McDonald%2C%20Laurie_The%20Dying%20Swan.mp4
---
Description for The Dying Swan

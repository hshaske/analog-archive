---
title: 'Letter to 1972 Women''s Video Festival Participants '
itemType: ephemera
poster: /archive/media/1972-kitchen-letter.png
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
pdfembed: thekitchennywvflettertoparticipants1972/mode/2up
---
Letter to 1972 Women's Video Festival Participants 

Signed Susan Milano & Shridhar Bapat cc: The Vasulkas

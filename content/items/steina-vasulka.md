---
title: 'STEINA '
itemType: gallery
poster: /archive/media/steina-vasulka-profile-pic.jpg
images:
  - image: >-
      https://archive.org/download/steina-vasulka-profile-pic/Steina%20Vasulka%20-%20profile%20pic.jpg
  - image: /archive/media/steina-orka.jpg
  - image: /archive/media/steina-violin-power.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
STEINA 

What do you do when you need to put together a bio for an old friend you haven’t seen for years who has reached the point in life, where her fame, notoriety and accomplishment as an artist, have grown to the point where she no longer needs to use a last name?  

For one thing, you recognize that so many versions of her journey or at least parts of it have already been put to paper, that producing something comprehensive would be a Herculean effort that would most surely leave something important out, or that actually, anything you might say would fall far short of the words already expressed by more articulate and knowledgeable experts in the field.  Hello STEINA!

I remember well when I met her in NYC in 1971, brought to the loft she and Woody had on 14th Street by John Reilly, to take notes for an interview he was doing for a local newspaper.  I was already learning about and working with portable video at Global Village, but I’d never seen anything like this.  The place was a morass of electronic equipment, with an occasional nod to domesticity.  A free-standing metal cabinet that might be suitable for storing office supplies held a precious collection of fine china along with cables and tools.

While I can’t recall specifically what tapes we looked at that night I believe we saw some samples of the experiments she and Woody were working on with synthetic image generation, as well as some “conventional” counter-culture footage.  I make this distinction because while early beginnings were documentary in nature, her work soon moved dramatically toward the manipulation of electronic imagery, and the creation of extraordinary installations of sound and sculpture which have rightfully brought her international fame and recognition.

What I can bring to the table that has been missing from every other account of her life’s work is the fact that she had the vision to recognize that many women, who like both of us, were being drawn into the video revolution that was taking place in 1971, were being overlooked.  She had the idea to have a video festival that would feature works of women and she asked me if I would organize it.  The first one, which came to be known as the Women’s Video Festival, was held at The Kitchen, multi-media she co-founded with Woody Vasulka, to full-house crowds, every night and it took off like wildfire.             (Written by Susan Milano)

For a more thorough look at Steina’s life and work, please see below where there is a narrative biography that was prepared for a show she did at the Burchfield Penney Art Center in 2011, and a Timeline from artnet,com 



STEINA: INVOLVING PEOPLE INTO THIS MAGIC

Steina is a pioneer of what we now consider routine in daily visual experience. Born in Iceland, Steina brings a uniquely musical visual approach with complex experimentation in electronic imaging to her art. This exhibition included seven of Steina's seminal video installations spanning her 40 year career.

Steina's love affair with art began as a young girl growing up in Reykjavik, Iceland. She began playing violin and attended every concert, play, opera, and gallery she could. In 1959, she received a scholarship from the Czechoslovak Ministry of Culture to attend the State Music Conservatory in Prague. While studying in Prague, she met Woody Vasulka and the couple married in 1964. In 1965, they moved to New York City where Steina worked as a freelance musician and Woody worked as filmmaker.

Through his film contacts, Woody came across video in 1969. Of that discovery Steina states: Both our lives where changed forever. Woody introduced me to his new discovery — what a rush! It was like falling in love; I never looked back. As soon as I had a video camera in my hand — as soon as I had that majestic flow of time in my control — I knew I had my medium.

For the Vasulkas, in the early days of video, everything was an installation or an environment. These environments included "live cameras" or "live switching of tapes" and featured multiple screens that were typically stacks of monitors along with several tape players. Steina and Woody created machines that allowed cameras to find images that humans could not. With the development of video projectors and computer imagery, these environments expanded the transformation of image and sound to larger, more diverse settings. Artists now had additional media — monitors, film screens, translucent surfaces, walls of various skins — through which to present their work.

Presented by Cannon Design with support from the National Endowment for the Arts

https://www.burchfieldpenney.org/exhibitions/exhibition:06-10-2011-09-25-2011-steina-involving-people-into-this-magic/



 

STEINA

1940

Born in Reykjavik, Iceland

1959

She studied violin and music theory, and in 1959 received a scholarship from the Czechoslovak Ministry of Culture to attend the State Music Conservatory in Prague.

1964

Woody Vasulka and Steina married in Prague in 1964, and shortly thereafter she joined the Icelandic Symphony Orchestra.

1965

After moving to the United States in 1965 she worked in New York City as a freelance musician.

1969

She began working with video in 1969, and since then her various tapes and installations have been exhibited in USA, Europe and Asia. Although her main thrust is in creating videotapes and installations, Steina has recently become involved in interactive performance in public places, playing a digitally adapted violin to move video images displayed on large video projectors.

1971

In 1971 she co-founded The Kitchen, an Electronic Media Theater in New York. Steina has been an artist-in-residence at the National Center for Experi-ments in Television, at KQED in San Francisco, and at WNET/Thirteen in New York.

1988

In 1988 she was an artist-in-residence in Tokyo on a U.S./Japan Friendship Committee grant. She has received funding from the New York State Council on the Arts, the National Endowment for the Arts, the Corporation for Public Broadcasting, the Guggenheim Foundation, the Rockefeller Foundation, the American Film Institute and the New Mexico Arts Division.

1993

In 1993 she co-curated with Woody the exhibition and catalogue, Eigenwelt der Apparatewelt (Pioneers of Electronic Art) for Ars Electronica in Linz, Austria.

1992–1995

She received the Maya Deren Award in 1992 and the Siemens Media Art Prize in 1995.

1996

In 1996 she served as the artistic co-director and software collaborator at STEIM (Studio for Electronic Instrumental Music) in Amsterdam. In 1996 Steina and Woody showed eight new media installations at the San Francisco Museum of Modern Art, an exhibition repeated in Santa Fe, New Mexico a few months later.

1997

Her installation, titled Orka, was featured in the Icelandic Pavilion at the 1997 Venice Biennale.

1999

In 1999 Steina showed three installations in three countries: Nuna in Albuquerque, New Mexico, Textures in Reykjavik, Iceland and Machine Vision in Milano, Italy.

2000

She created two installations for the Art Festival 2000 in Reykjavik, Iceland.

2001

In 2001 she was invited to festivals in Norway, Russia, Estonia, Portugal, Montreal, England and Italy.

2002

Between July and October of 2002 she realized four installations in four locations in her hometown of 22 years, Santa Fe.

2004–2005

Steina had a solo exhibition at EVO Gallery in 2004, and in 2005, she was part of the Museum of Fine Arts’ (Santa Fe) exhibition, Embodied: Seven Studies in Video.

Steina is represented by EVO Gallery in Santa Fe.



http://www.artnet.com/artists/steina-vasulka/biography

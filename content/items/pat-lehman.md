---
title: 'Pat Lehman '
itemType: gallery
poster: /archive/media/pat-lehman-profile-pic_v2.jpeg
images:
  - image: /archive/media/pat-lehman-profile-pic_v2.jpeg
  - image: /archive/media/pat-lehman-profile-pic.jpeg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
BIO: 

How I Became a Video Artist in the 70’s

When I was in art school in the late 60’s at Colorado State University, I learned that I liked movement and light more than the flat image made on canvas. To create moving paintings, I started painting on clear leader film not unlike the work of Stan Brackhage. It was the love of light and the projected image which led to the study of filmmaking. It was the marriage of both of these ideas that led me to study computer animation.

While in grad school in 1970, I discovered a small start-up company called Computer Image Corporation in Denver, Colorado. They were young idealistic engineers trying to create analog computer animation for commercial applications but had no idea how art could enhance their work. I persuaded them they needed an artist to direct their animations and became their first Art Director. Some of the first animated projects were for Sesame Street and animated logos for television using their Scanimate Computer.

My introduction to video as a medium came when C.I.C. made the transition from film to video for their computer animation work. The changeover changed my life! I instantly fell in love with the vibrant light of video and its malleability. Computer animation and video seemed to be the perfect amalgam of both media. My M.F.A. thesis in 1972 was “Computer Animation for Film and Video Tape” based on the work I did at C.I.C. with an accompanying film called “Drug Abuse” to be used as a 60 second public service announcement made on the Scanimate Computer.



Video in the 70’ - First Portapack

During the 70’s I experimented with the beautiful black and white imagery making many unedited tapes of events which became abstract images, plus tons of abstract feedback images. 

As an “early adaptor” of technology, my next step was to acquire the first 1/2” video portapack equipment in Denver in 1972. Portable video could allow the independent filmmaker/videographer a medium that was accessible, affordable, democratic, and inexpensive compared to motion picture film equipment and film processing. Even better for the artist, it offered instant gratification to be able to see what was recorded immediately instead of waiting for film be be processed and audio to be added to the film. I could see video becoming my new canvas for creating a new art form - Video Art with the creation of the moving image with sound, both abstract and documentary. To make money with this incredible device, I created a business called Videographics to make videotape depositions for the medical profession. Not only was this interesting work, but it paid for my equipment in no time! I had no competition as no one else in the country was doing this yet and I had the only portable video equipment in the area.

Next step was to set up a Dept. of Film and Video at UMass Boston in 1972 with the emphasis on making Video Art and Super 8 documentaries.

I directed my students to utilize the camera and tape to create and record images and situations that would be impossible to duplicate using any other medium. To this day I regret that a lot of the truly innovative work was erased and the tape reused just because it could be. I liked the idea of creating art that was temporal and not object based. Experience meant more than creating something that would populate the world with another object.



Cinema Vérité 1/2” video

While I could not pursue computer animation as an individual artist, except through limited affiliation with university computer labs, I would be able to pursue “cinema verite” from courses I took at M.I.T. from one of the founders of the cinema verite movement, Richard (Ricky) Leacock whose method was to shoot documentaries by interfering as little as possible to capture the raw vitality of a situation. This was only possible with the new, small equipment that was unobtrusive and did not require a film crew, scripts, or a serious budget to afford pre-production, shooting, and post-production services.

Using “cinema verite” documentary style in the 70’s, I sought to explore people making their own kind of “art” - better known as “Outsider Art”.  As we were deep into the Sexual Revolution, I first sought out strippers, both male and female and asked them how they saw their work as actual “performance art,” and how they felt about the sexual objectivization of their bodies in their work, and how their work affected their personal lives as straight or gay.

In 1975 I embarked in another direction for a Carnegie Foundation Grant to make a series of interviews of successful women in the arts called Career Counciling for Women in the Arts. It had been painfully obvious to me working in the art of film and video that there was an inherent sexism working against women getting meaningful, well-paid positions in the arts. I wanted to interview women who had achieved their goals and what it took to get there with the hopes it would inspire young women seeking careers in the area of arts management. These tapes were not video art, but produced to be educational. The quality of the video is very uneven and often not good but the information is there.



Video in the ’80’s changed the aesthetic

Always Interested in properties of video as art which allowed me to exploit the limitations of the medium: the absence of color…the lack of three-dimensional depth. It was my goal to make a virtue of what was lacking in black and white video to develop a new and distinctive art form as video art. Similar to the early internet, I could see that video art could empower new artists, especially women, to challenge the old male power structures; an idea that lives on today, 50 years later, but unfortunately, not a lot has changed.

The introduction of color VHS portable recorders led to greater realism and I believe it had a corresponding loss in artistry.  

As the VHS tapes and recorders became available in the late 70’s, the teaching of Video Art switched to the new format of cassette tapes that could be played at home. By the early 80’s VCRs were so inexpensive that most homes had one to watch movies at home. The VHS portapacks were somewhat lighter than the old 1/2” units, but the big advantage was color video was available for the first time for the video artist/filmmaker. 



Community Activism - Public Access TV

In 1977 I helped organize the local group of the National Federation of Local Cable Programmers (NFLCP) in Denver. Cable companies were mandated to make their production studios available to local citizens to make local programming for their citizens. This was part of the deal when the government awarded cable franchises. It was the perfect opportunity for me and my students to experiment with television studio work and editing. We made weekly programs for about 15 months. Sadly, I no longer see community organized video projects. It was true democracy at work. TV for the people by the people. The best parody of public access TV can be seen in the hilarious “Wayne’s World” movie (1992).



VHS Documentary

“The First Time” (1984) At the end Sexual Revolution we saw a change in attitudes about sex in general. I decided to make a tape using my students and art colleagues about their first sexual experience. This was for fun and a discussion of a special event we all share but something we seldom discuss. This was shortly before the AIDS epidemic that would take many of our fellow artists. Two of the students that appear in The First Time died of AIDS a couple years after this tape was made.



Computer Animation 

“Drug Abuse” (1971) Public Service Announcement 60 Second spot designed to be run as a public service on TV. It is one of the first computer animations I made when working at Computer Image Corp. This piece was entered in the Cannes Film Festival and exhibited in the Director’s Fortnight which was to showcase new creative visual effects in film. Computer animation had not yet entered any part of the mainstream in film production.

“Video Verite” (1974) 10 min. This is a feminist computer animatied video with live action made at Computer Image Corp. using a Scanimate Computer. It is semi-autobiographical of a woman in conflict with her male dominated environment, and aspirations thwarted by roadblocks along the way.

Video Vitae was entered in the Women’s Video Festival organized by 	Susan Milano. This tape went on to win several first place awards in numerous film festivals in the U.S., Europe, and Australia.

It was very exciting to see that a video festival was organized around the      idea of promoting video made by women. By entering, I was hoping my video about a woman feeling constrained by the conventions of society would actually resonate with other women.

It never ceased to amaze me that my name, being androgynous, gave me an advantage in entering film and video festivals. I always thought a work should stand on its own whether or not it was made by a man or a woman. On more than one occasion, when I was invited to attend a festival or judge a film/video competition, there would be hesitation or invitation withdrawn.

Fractile 1 & 2 (1986) Abstract computer animation using a hybrid computer at the Univ. of Kansas. This video was entered in many festivals.

---
title: Descartes
itemType: video
poster: /archive/media/descartes.jpg
images:
  - {}
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
annotations:
  - end: '00:00:00'
    files:
      - file: /archive/media/elkholy-sharin_joanne-kyger-descartes.pdf
    images:
      - {}
    start: '00:00:00'
  - body: Videotape Program at the Whitney Museum including Kyger's Descartes
    end: '00:01:00'
    files:
      - file: >-
          /archive/media/nytimes-videotape-program-at-the-whitney-includes-kyger-s-descartes-1971.12.10.pdf
    start: '00:01:00'
mediaUrl: 'https://vimeo.com/68494299'
---


---
title: Janice Carrick
itemType: gallery
images:
  - {}
collections:
  - {}
resources:
  - file: /archive/media/janice-carrick-feminist-video-project-off-our-backs.jpg
keywords:
  - {}
related:
  - items:
      - {}
---
Member of the Feminist Video Project 

Based at the Washington Area Women's Center in Washington, D.C.

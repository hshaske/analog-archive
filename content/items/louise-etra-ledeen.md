---
title: 'Louise (Etra) Ledeen '
itemType: gallery
poster: /archive/media/louise-etra-profile-pic-1.jpg
images:
  - image: >-
      https://archive.org/download/louise-etra-ledeen-profile-pic/Louise%20%28Etra%29%20Ledeen%20-%20Profile%20pic.jpg
collections:
  - {}
resources:
  - {}
keywords:
  - {}
related:
  - items:
      - {}
---
Shortly after graduating Hunter College with a degree in Art, Louise Etra became an Assistant Exhibition Coordinator at the CUNY Graduate Center, from 1974-1976, focusing on large scale sculpture installations and extensive exhibitions and symposia of historical New York architectural landmarks.

During this time, Louise began her work in video as an art form, drawn to the color of transmitted light and the ability to create kinetic, dynamic “paintings” by exploring temporal space and movement.   With her partner, Steve Rutt, and first husband, Bill Etra, they began working on building the Rutt/Etra (R/E) video synthesizers.    

The R/E analog video synthesizers are image processing devices that create animation in realtime by manipulating the analog video raster.  The combination of realtime camera images, with realtime waveform effects was a powerful tool for creating dynamic video art work.   Modified R/E Video Synthesizers are still being used today by new generations of artists, who have improved on the design by incorporating computer technology to create much more repeatable and complex effects, programmed for longer durations.

In the early to mid-1970s began Louise began participating in and curating international exhibitions of technology-based art – video, audio and computer graphics, as an attempt to bring these forms of art into the mainstream art community.

In the 1980’s, Louise Etra remarried and when she and her second husband David had their daughter Hannah, Louise changed her name to Louise Ledeen.

During this time, Louise shifted her focus to developing new tools, systems and later, cloud services, to optimize the creative process.  Louise was one of the founders of Diaquest, a small company that created devices for video output from PCs, Mac, SGI and other UNIX computer graphics systems.   At SuperMac and Radius, Louise learned more about product management and product marketing for video devices and software for early Macintosh computers.

When Louise joined Silicon Studio, a division of Silicon Graphics, Inc. (SGI). her focus was working with strategic partners to maximize their 2D, 3D graphics and editing and compositing applications on the SGI platform and worked with Media and Entertainment clients to bring their requirements to engineering teams to overcome bottlenecks and optimize collaboration.  She worked with Philips/Thomson on developing the first realtime 24 fps digital telecine.  Working with Matsushita/Panasonic, she helped drive the SGI development for the first computer-based system for news production.   The power of the computer systems, networks, the commoditization of computer graphics products, and the advent of digital motion images, meant that more higher resolution content was being generated, digital image files grew in quantity and size, Louise began focusing on the data management and data security issues, which became the next big bottlenecks, in the creative workflow, especially as more collaboration was taking place, across geographical boundaries.  At that point, Louise joined NetApp, a leading and innovative storage and data management company, continuing her work with strategic partners to create more beneficial solutions and cloud services.   Louise became a refuge from Silicon Valley in 2018.

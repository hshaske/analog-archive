---
title: 1973 Festival
groups:
  - items:
      - item: 1973 Program
    title: Program
  - items:
      - item: >-
          <iframe title="vimeo-player"
          src="https://player.vimeo.com/video/227462200" width="640"
          height="480" frameborder="0" allowfullscreen></iframe>
      - item: Sexual Fantasy Party
      - item: 'Gay Pride March / Rally '
      - item: 'Priest and the Pilot '
      - item: Women Who've Lived Through Illegal Abortions
      - item: Dressing Up
      - item: 'Vasant Rai '
      - item: 'Pelicans '
      - item: 'King Cotton '
      - item: 'I Am a Woman '
      - item: National Lesbian Conference April 1973
      - item: Women on Women
      - item: 'Golden Voyage '
    title: Videos
  - items:
      - item: 'Charlotte Moorman Performances '
    title: Performances
  - items:
      - item: 'Going Out Guide: Ms. Message '
      - item: 'The 2nd Annual Women''s Video Festival '
      - item: 'Videotape: Woman''s Video Festival '
    title: Historic Reviews
  - items:
      - item: 'LoveTapes Collective Gay Pride Screening '
      - item: '1973 Kitchen Calendar '
    title: Ephemera
tableFields:
  - {}
---


---
title: 1976 Festival
groups:
  - items:
      - item: '1976 Program '
      - item: '1976 Longsheet '
    title: Program
  - items:
      - item: Hydroglyphs Early Morning Drift
      - item: The Dying Swan
      - item: Narcissicon
      - item: 'Flo Kennedy '
      - item: 'Forest of Canes '
      - item: 'Fifty Wonderful Years '
      - item: Video Vitae
      - item: Miss Eve
      - item: 'It''s a Living: When I Was a Worker Like LaVerne'
      - item: 'My Bubi, My Zada'
      - item: 'Women of Northside Fight Back '
    title: Videos
  - items:
      - item: 'Antique with Video Ants and Generations of Dinosaurs '
    title: 'Video Sculptures, Installations, and Environments '
  - title: 'Performances '
  - items:
      - item: 'Women''s Video Festival: Portrait of the Artist as a Young Woman'
    title: 'Historic Reviews '
  - title: Ephemera
tableFields:
  - key: date
    title: Date
---


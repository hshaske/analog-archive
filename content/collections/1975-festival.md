---
title: 1975 Festival
groups:
  - items:
      - item: '1975 Agenda '
      - item: '1975 Program '
    title: Program
  - items:
      - item: Lady of the Lake
      - item: 'Barbara Buckner - Sample '
      - item: 'Lesbianism/Feminism '
      - item: 'Chicken (on Foot) '
      - item: 'Harriet '
      - item: Portapak Conversation
      - item: Words 1-5
      - item: 'Heraldic View '
    title: 'Videos '
  - items:
      - item: 'Video Swing '
      - item: 'Glitter Room '
      - item: 'Video Toys '
    title: 'Video Sculptures, Installations, and Environments '
  - title: 'Performances '
  - items:
      - item: 'Women''s Video: Rooms with Many Views'
    title: 'Historic Reviews '
tableFields:
  - {}
---


---
title: 1972 Festival
groups:
  - items:
      - item: 1972 Agenda
    title: Program
  - items:
      - item: 'Lesbian Mothers '
      - item: 'Tattoo '
      - item: 'Mondctvtano Mission of Peace to Cuba '
      - item: Descartes
      - item: Let It Be
      - item: 'Cape May '
    title: Videos
  - items:
      - item: 'Cycles '
      - item: 'Tee Pee Video Space Troupe: Totem Tapes - Elsa Tambellini'
    title: 'Performances '
  - items:
      - item: 'Counter Vulture: Women''s Video Festival '
      - item: '"Transsexuals" review in Radical Software '
      - item: 'Women on Tape: Spinning Tails and Tailspins '
      - item: >-
          Report from New York: Women's Video Festival at The Kitchen (Sept.
          14-30, 1972)
      - item: 'Video Women''s Place is in The Kitchen '
      - item: 'Video Theatre: Women''s Festival at Mercer Arts Center'
    title: 'Historic Reviews '
  - items:
      - item: Off Our Backs
      - item: 'Letter to 1972 Women''s Video Festival Participants '
      - item: 'Women''s Video Festival Press Release '
      - item: 'Movie Journal '
    title: Ephemera
tableFields:
  - {}
---

